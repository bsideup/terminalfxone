package org.semaco.fxw.terminal.fix.fxone.fields;

import quickfix.StringField;

public class FxOneMktID extends StringField {
    
    public static final int FIELD = 5000;

    public FxOneMktID() {
        this("");
    }

    public FxOneMktID(String data) {
        super(FIELD, data);
    }
}

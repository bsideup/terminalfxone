package org.semaco.fxw.terminal.fix.fxone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;
import quickfix.field.MsgType;
import quickfix.field.Password;
import quickfix.field.ResetSeqNumFlag;
import quickfix.field.Username;

public class FxOneApplication implements Application {
    
    public static interface Callback {
        void onLogon(boolean isLogon);
    }

    private static final Logger log = LoggerFactory.getLogger(FxOneApplication.class);

    private final TerminalFixFxOne.Cracker cracker;
    
    private final SessionSettings settings;
    
    private final Callback callback;

    private final SessionID tradeSessionId;

    private final SessionID priceSessionId;
    
    private boolean isTradeLogon = false;

    private boolean isPriceLogon = false;

    public FxOneApplication(TerminalFixFxOne.Cracker cracker, SessionSettings settings, Callback callback,
                            SessionID tradeSessionId, SessionID priceSessionId) {
        this.cracker = cracker;
        this.settings = settings;
        this.callback = callback;
        this.tradeSessionId = tradeSessionId;
        this.priceSessionId = priceSessionId;
    }

    @Override
    public void onCreate(SessionID arg0) {
    }

    @Override
    public void toApp(Message arg0, SessionID arg1) throws DoNotSend {
    }

    @Override
    public void fromAdmin(Message msg, SessionID sid) throws FieldNotFound, IncorrectDataFormat,
            IncorrectTagValue, RejectLogon {
        try {
            cracker.crack(msg, sid);
        } catch (UnsupportedMessageType e) {
            //TODO log?
        }
    }

    @Override
    public void fromApp(Message msg, SessionID sid) throws FieldNotFound, IncorrectDataFormat,
            IncorrectTagValue, UnsupportedMessageType {
        cracker.crack(msg, sid);
    }

    @Override
    public void onLogon(SessionID sessionID) {
        if (sessionID == tradeSessionId) {
            isTradeLogon = true;
        } else if (sessionID == priceSessionId) {
            isPriceLogon = true;
        } else {
            return;
        }

        boolean allLoggedIn = isTradeLogon && isPriceLogon;
        
        if (!allLoggedIn) {
            return;
        }

        callback.onLogon(true);
    }

    @Override
    public void onLogout(SessionID sessionID) {
        if (sessionID == tradeSessionId) {
            isTradeLogon = false;
        } else if (sessionID == priceSessionId) {
            isPriceLogon = false;
        } else {
            return;
        }

        callback.onLogon(false);
    }

    @Override
    public void toAdmin(Message msg, SessionID sessionID) {
        final String msgType;
        try {
            msgType = msg.getHeader().getString(MsgType.FIELD);
        } catch (FieldNotFound e) {
            log.error("failed to get field from header", e);
            return;
        }

        if (MsgType.LOGON.equalsIgnoreCase(msgType)) {
            try {
                String login = settings.get(sessionID).getString("login");

                if (login.length() > 0) {
                    msg.setField(new Username(settings.get(sessionID).getString("login")));
                }

                msg.setField(new Password(settings.get(sessionID).getString("password")));
                msg.setField(new ResetSeqNumFlag(sessionID == tradeSessionId ? true : true)); //FIXME temporary
            } catch (Exception e) {
                log.error("failed to process message " + msg, e);
            }
        }
    }
}

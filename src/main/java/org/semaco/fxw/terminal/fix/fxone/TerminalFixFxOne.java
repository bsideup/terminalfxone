package org.semaco.fxw.terminal.fix.fxone;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.semaco.fxw.terminal.fix.TerminalFixAdapter;
import org.semaco.fxw.terminal.fix.fxone.fields.FxOneMktID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;
import quickfix.field.*;
import quickfix.fix44.MarketDataRequest;
import quickfix.fix44.MarketDataSnapshotFullRefresh;
import quickfix.mina.NetworkingOptions;
import quickfix.mina.ssl.SSLSupport;

import java.io.File;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class TerminalFixFxOne extends TerminalFixAdapter {

    private static final Logger log = LoggerFactory.getLogger(TerminalFixFxOne.class);

    private SessionSettings settings;
    
    private SessionID tradeSessionId;
    
    private SessionID priceSessionId;
    
    private Initiator initiator;
    
    private MessageStoreFactory messageStoreFactory;
    
    private ScreenLogFactory logFactory;
    
    private DefaultMessageFactory messageFactory;
    
    private Application application;
    
    private AtomicInteger uid = new AtomicInteger();
    
    private MDReqID mdReqId = null;
    
    private String account;
    
    private String marketId;

    private boolean debug = false;

    protected abstract void watchDebugFile(File file);

    protected abstract String getClientId();

    protected abstract String getSymbol();

    @Override
    public void logon() throws Exception {
        if (initiator != null) {
            initiator.stop();
        }

        Objects.requireNonNull(application, "application can't be null");
        
        initiator = new ThreadedSocketInitiator(application, messageStoreFactory, settings, logFactory, messageFactory);
        initiator.start();
    }

    @Override
    public void close() throws Exception {
        if (initiator != null) {
            initiator.stop(true);
        }
    }
    
    protected SessionSettings createInitialSettings(boolean debug) throws Exception {

        SessionSettings settings = new SessionSettings();
        settings.setString(SessionFactory.SETTING_CONNECTION_TYPE, SessionFactory.INITIATOR_CONNECTION_TYPE);
        
        settings.setLong(Initiator.SETTING_RECONNECT_INTERVAL, 2);

        settings.setString(Session.SETTING_START_TIME, "00:00:00");
        settings.setString(Session.SETTING_END_TIME, "00:00:00");
        settings.setBool(Session.SETTING_NON_STOP_SESSION, true);
        settings.setBool(Session.SETTING_USE_DATA_DICTIONARY, true);
        settings.setBool(Session.SETTING_RESET_ON_LOGON, false);
        settings.setString(Session.SETTING_DATA_DICTIONARY, "terminals/fix/FIX44.xml"); //FIXME

        settings.setBool(Session.SETTING_VALIDATE_USER_DEFINED_FIELDS, false);

        settings.setBool(NetworkingOptions.SETTING_SOCKET_KEEPALIVE, true);
        
        settings.setBool(ScreenLogFactory.SETTING_LOG_EVENTS, debug);
        settings.setBool(ScreenLogFactory.SETTING_LOG_INCOMING, debug);
        settings.setBool(ScreenLogFactory.SETTING_LOG_OUTGOING, debug);

        File fileStorePath = new File("./fix/fxone" + getClientId()).getCanonicalFile();
        if (!fileStorePath.getAbsoluteFile().exists()) {
            FileUtils.forceMkdir(fileStorePath);
        }
        settings.setString(FileStoreFactory.SETTING_FILE_STORE_PATH, fileStorePath.getAbsolutePath());
        
        return settings;
    }
    
    @Override
    public void update(JSONObject data) throws Exception {
        Boolean debug = data.optBoolean("debug");
        
        JSONObject sessionData = data.getJSONObject("session");

        SessionSettings settings = createInitialSettings(debug);

        final BeginString beginString = new BeginString("FIX.4.4");
        
        JSONObject itemSessionData = sessionData.getJSONObject("trade");
        SessionID tradeSessionId = new SessionID(
                beginString,
                new SenderCompID(itemSessionData.getString("senderCompID")),
                new TargetCompID(itemSessionData.getString("targetCompID")),
                "Session-trade"
        );

        Dictionary sessionDict = settings.get();
        sessionDict.setString(Initiator.SETTING_SOCKET_CONNECT_PORT, "" + itemSessionData.getInt("port"));
        sessionDict.setString(Initiator.SETTING_SOCKET_CONNECT_HOST, itemSessionData.getString("host"));
        sessionDict.setString(SSLSupport.SETTING_USE_SSL, itemSessionData.getBoolean("ssl") ? "Y" : "N");
        sessionDict.setString(Session.SETTING_HEARTBTINT, itemSessionData.optInt("heartBtInt", 30) + "");
        sessionDict.setString("login", itemSessionData.optString("login", ""));
        sessionDict.setString("password", itemSessionData.optString("password", ""));

        settings.set(tradeSessionId, sessionDict);

        itemSessionData = sessionData.getJSONObject("price");
        SessionID priceSessionId = new SessionID(
                beginString,
                new SenderCompID(itemSessionData.getString("senderCompID")),
                new TargetCompID(itemSessionData.getString("targetCompID")),
                "Session-price"
        );

        sessionDict = settings.get();
        sessionDict.setString(Initiator.SETTING_SOCKET_CONNECT_PORT, "" + itemSessionData.getInt("port"));
        sessionDict.setString(Initiator.SETTING_SOCKET_CONNECT_HOST, itemSessionData.getString("host"));
        sessionDict.setString(SSLSupport.SETTING_USE_SSL, itemSessionData.getBoolean("ssl") ? "Y" : "N");
        sessionDict.setString(Session.SETTING_HEARTBTINT, itemSessionData.optInt("heartBtInt", 5) + "");
        sessionDict.setString("login", itemSessionData.optString("login"));
        sessionDict.setString("password", itemSessionData.optString("password"));
        
        settings.set(priceSessionId, sessionDict);

        settings.setLong(Initiator.SETTING_SOCKET_CONNECT_PORT, itemSessionData.getInt("port"));
        settings.setString(Initiator.SETTING_SOCKET_CONNECT_HOST, itemSessionData.getString("host"));

        String account = data.getString("account");
        update(account, debug, data.getString("marketId"), settings, tradeSessionId, priceSessionId);
    }
    
    protected void update(String account, Boolean debug, String marketId, SessionSettings sessionSettings, SessionID tradeSessionId, SessionID priceSessionId) {
        this.account = account;
        this.debug = debug;
        this.marketId = marketId;
        
        this.settings = sessionSettings;
        
        this.tradeSessionId = tradeSessionId;
        this.priceSessionId = priceSessionId;
        
        messageStoreFactory = new FileStoreFactory(settings);
        logFactory = new ScreenLogFactory(settings);
        messageFactory = new DefaultMessageFactory();
        
        FxOneApplication.Callback callback = new FxOneApplication.Callback() {
            @Override
            public void onLogon(boolean isLogon) {
                TerminalFixFxOne.this.onLogon(isLogon);
            }
        };

        application = new FxOneApplication(new Cracker(), sessionSettings, callback, tradeSessionId, priceSessionId);

        if (debug) {
            watchDebugFile(new File("./testADS.json"));
        }
    }

    @Override
    public void triggerStageEvent(Message message) throws Exception {
        message.setField(getTransactionTime());
        sendMessage(message, tradeSessionId);
    }
    
    @Override
    public Message getOrderMessage(ClOrdID orderId, double qty, double div, JSONObject data) throws Exception {
        char orderType = data.getString("orderType").charAt(0);
        String symbol = data.getString("symbol");
        double lotSize = data.getDouble("lotSize");
        double price = data.optDouble("price");
        
        return getOrderMessage(orderId, qty, div, orderType, symbol, lotSize, price);
    }
    
    public Message getOrderMessage(ClOrdID orderId, double qty, double div, char orderType, String symbol, double lotSize, double price) throws Exception {
        quickfix.fix44.NewOrderSingle result = new quickfix.fix44.NewOrderSingle(
                orderId,
                new Side(Side.BUY),
                getTransactionTime(),
                new OrdType(orderType)
        );
        result.setField(new Symbol(symbol));
        result.setField(new OrderQty(qty * lotSize));
        result.setField(new TimeInForce(TimeInForce.DAY));

        if(orderType == OrdType.LIMIT) {
            result.setField(new Price(price));
        }
        
        //result.setField(new Account(account));

        /*
        if (orderType != OrdType.MARKET) {
            result.setField(new DiscretionOffsetValue(div));
        }
        */

        //result.setField(getCurrency());

        //result.setField(new QuoteID("QID" + uid.incrementAndGet()));
        
        return postProcessMessage(result);
    }

    @Override
    public boolean marketDataReject(String mdSymbol) throws Exception {
        if (mdReqId == null) {
            return true;
        }
        
        MarketDataRequest result = new MarketDataRequest(
                mdReqId,
                new SubscriptionRequestType(SubscriptionRequestType.DISABLE_PREVIOUS_SNAPSHOT_PLUS_UPDATE_REQUEST),
                new MarketDepth(0)
        );
        
        //WTF?
        //mdReq.setChar(265, '0');
        result.setField(new MDUpdateType(MDUpdateType.FULL_REFRESH));
        
        quickfix.fix44.MarketDataRequest.NoMDEntryTypes typeGroup = new quickfix.fix44.MarketDataRequest.NoMDEntryTypes();
        typeGroup.set(new MDEntryType(MDEntryType.BID));
        result.addGroup(typeGroup);
        
        typeGroup.set(new MDEntryType(MDEntryType.OFFER));
        result.addGroup(typeGroup);
        
        quickfix.fix44.MarketDataRequest.NoRelatedSym symGroup = new quickfix.fix44.MarketDataRequest.NoRelatedSym();
        symGroup.set(new Symbol(mdSymbol));
        result.addGroup(symGroup);
        
        return sendMessage(result, priceSessionId);
    }

    @Override
    public void marketData(String symbol) throws Exception {
        mdReqId = new MDReqID("" + uid.incrementAndGet());

        MarketDataRequest result = new MarketDataRequest(
                mdReqId,
                new SubscriptionRequestType(SubscriptionRequestType.SNAPSHOT_PLUS_UPDATES),
                new MarketDepth(1)
        );

        //WTF?
        //result.setChar(265, '0');
        result.setField(new MDUpdateType(MDUpdateType.FULL_REFRESH));

        result.setField(new IntField(5003, 3));
        
        quickfix.fix44.MarketDataRequest.NoMDEntryTypes bidTypeGroup = new quickfix.fix44.MarketDataRequest.NoMDEntryTypes();
        bidTypeGroup.set(new MDEntryType(MDEntryType.BID));
        result.addGroup(bidTypeGroup);

        quickfix.fix44.MarketDataRequest.NoMDEntryTypes offerTypeGroup = new quickfix.fix44.MarketDataRequest.NoMDEntryTypes();
        offerTypeGroup.set(new MDEntryType(MDEntryType.OFFER));
        result.addGroup(offerTypeGroup);
        
        result.setField(new NoRelatedSym('1'));
        
        quickfix.fix44.MarketDataRequest.NoRelatedSym symGroup = new quickfix.fix44.MarketDataRequest.NoRelatedSym();
        symGroup.set(new Symbol(symbol));
        result.addGroup(symGroup);

        sendMessage(result, priceSessionId);
    }

    @Override
    public Message sendCloseOrderMessage(ClOrdID clOrdID, Side side, String symbol, double qty) throws Exception {
        quickfix.fix44.NewOrderSingle result = new quickfix.fix44.NewOrderSingle(
                clOrdID,
                side,
                getTransactionTime(),
                new OrdType(OrdType.MARKET)
        );

        result.setField(new Account(account));
        result.setField(new Symbol(symbol));
        result.setField(getCurrency());
        result.setField(new OrderQty(qty));
        result.setField(new TimeInForce(TimeInForce.IMMEDIATE_OR_CANCEL));
        result.setField(new QuoteID("QID" + uid.incrementAndGet()));
        
        sendMessage(result, tradeSessionId);
        
        return result;
    }
    
    protected Message postProcessMessage(Message message) {
        message.setField(new FxOneMktID(marketId));
        return message;
    }
    
    protected boolean sendMessage(Message message, SessionID sessionID) throws SessionNotFound {
        message = postProcessMessage(message);
        return Session.sendToTarget(message, sessionID);
    }

    protected TransactTime getTransactionTime() {
        return new TransactTime(new Date());
    }

    protected Currency getCurrency() {
        return new Currency(getSymbol().substring(0, 3));
    }

    protected class Cracker extends MessageCracker {

        @Override
        public void crack(Message message, SessionID sessionID) throws UnsupportedMessageType, FieldNotFound, IncorrectTagValue {
            log.warn("{} {}", message.getClass(), message.toString());
            super.crack(message, sessionID);
        }
        
        public void onMessage(quickfix.fix44.TradingSessionStatus message, SessionID sessionID) {
            try {
                log.info("trading session status: {}", message.getString(TradSesStatus.FIELD));
            } catch (FieldNotFound ignored) {
            }
        }

        public void onMessage(quickfix.fix44.ExecutionReport report, SessionID sessionID) {

            try {
                if (debug) {
                    System.out.println("!!! ClOrdID: " + (report.isSetClOrdID() ? report.getClOrdID().getValue() : "--") + "; OrigClOrdID:" + (report.isSetOrigClOrdID() ? report.getOrigClOrdID().getValue() : "--"));
                    System.out.println("!!! OrdStatus: " + (report.isSetOrdStatus() ? report.getOrdStatus().getValue() : "--") + "; AllocID:" + (report.isSetField(AllocID.FIELD) ? report.getField(new AllocID()).getValue() : "--"));
                    System.out.println("!!! OrdRejReason: " + (report.isSetOrdRejReason() ? report.getOrdRejReason().getValue() : "--") + "; Text:" + (report.isSetText() ? report.getText().getValue() : "--"));
                }
                onDeal(
                        System.nanoTime(),
                        report.getClOrdID(),
                        report.getOrdStatus(),
                        report.getExecType(),
                        report.isSetLastQty() ? report.getLastQty() : null,
                        report.isSetLastPx() ? report.getLastPx() : null,
                        report.getSide(),
                        report.isSetOrdRejReason() ? report.getOrdRejReason() : null
                );
            } catch (FieldNotFound e) {
                log.error("failed to get field", e);
            }
        }

        public void onMessage(quickfix.fix44.MarketDataSnapshotFullRefresh mq, SessionID sessionID) {
            try {
                if (!mq.getSymbol().getValue().replaceAll("[/]", "").equals(getSymbol())) {
                    return;
                }
                int count = mq.getNoMDEntries().getValue();
                quickfix.fix44.MarketDataSnapshotFullRefresh.NoMDEntries group =
                        new quickfix.fix44.MarketDataSnapshotFullRefresh.NoMDEntries();
                double bid = 0;
                double ask = 0;
                for (int i = 1; i <= count; i++) {
                    mq.getGroup(i, group);
                    if (group.getQuoteCondition().getValue().equals("A")) {
                        switch (group.getMDEntryType().getValue()) {
                            case MDEntryType.BID:
                                bid = group.getMDEntryPx().getValue();
                                break;
                            case MDEntryType.OFFER:
                                ask = group.getMDEntryPx().getValue();
                                break;
                        }
                    }
                }

                onUpdateState(bid, ask);

            } catch (Exception e) {
                log.error("failed to process message " + mq, e);
            }
        }

    }

}

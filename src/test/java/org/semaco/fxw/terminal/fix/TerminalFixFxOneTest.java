package org.semaco.fxw.terminal.fix;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.semaco.fxw.terminal.fix.fxone.TerminalFixFxOne;
import org.slf4j.LoggerFactory;
import quickfix.Message;
import quickfix.field.*;

import java.io.File;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TerminalFixFxOneTest {
    
    TerminalFixAdapter adapterMock;

    TerminalFixFxOne adapter;

    @Before
    public void setUp() throws Exception {
        
        adapterMock = mock(TerminalFixAdapter.class);

        adapter = new TerminalFixFxOne() {
            @Override
            protected void onDeal(long nanoTime, ClOrdID orderId, OrdStatus ordStatus, ExecType exectype, LastQty lastQty, LastPx lastPx, Side side, OrdRejReason ordRejReason) {
                super.onDeal(nanoTime, orderId, ordStatus, exectype, lastQty, lastPx, side, ordRejReason);

                adapterMock.onDeal(nanoTime, orderId, ordStatus, exectype, lastQty, lastPx, side, ordRejReason);
            }

            @Override
            protected void onUpdateState(double bid, double ask) {
                super.onUpdateState(bid, ask);

                adapterMock.onUpdateState(bid, ask);
            }

            @Override
            protected void watchDebugFile(File file) {
                //do nothing
            }

            @Override
            protected String getClientId() {
                return "1";
            }

            @Override
            protected String getSymbol() {
                return "EURUSD";
            }
        };

        JSONObject settings = jsonObjectFromFile("settings.json");
        adapter.update(settings);

        adapter.logon();
        Thread.sleep(5000);
    }

    @After
    public void tearDown() throws Exception {
        adapter.close();
    }

    @Test
    public void testNormalMarketScenario() throws Exception {
        ClOrdID ordID = new ClOrdID(UUID.randomUUID().toString());

        JSONObject orderData = jsonObjectFromFile("marketOrder.json");

        Message orderMessage = adapter.getOrderMessage(ordID, 1, 10, orderData);

        adapter.triggerStageEvent(orderMessage);

        verify(adapterMock, timeout(10000).times(1)).onDeal(
                anyLong(),
                eq(ordID),
                eq(new OrdStatus(OrdStatus.NEW)),
                any(ExecType.class),
                any(LastQty.class),
                any(LastPx.class),
                eq(new Side(Side.BUY)),
                eq((OrdRejReason) null)
        );
    }

    @Test
    public void testNormalLimitScenario() throws Exception {
        ClOrdID ordID = new ClOrdID(UUID.randomUUID().toString());

        JSONObject orderData = jsonObjectFromFile("limitOrder.json");

        Message orderMessage = adapter.getOrderMessage(ordID, 1, 10, orderData);

        adapter.triggerStageEvent(orderMessage);

        verify(adapterMock, timeout(10000).times(1)).onDeal(
                anyLong(),
                eq(ordID),
                eq(new OrdStatus(OrdStatus.NEW)),
                any(ExecType.class),
                any(LastQty.class),
                any(LastPx.class),
                eq(new Side(Side.BUY)),
                eq((OrdRejReason) null)
        );
    }
    
    @Test
    public void testQuotes() throws Exception {
        String symbol = "EURUSD";

        adapter.marketData(symbol);

        verify(adapterMock, timeout(60000).atLeastOnce()).onUpdateState(
                eq(0.0),
                eq(0.0)
        );
    }
    
    protected JSONObject jsonObjectFromFile(String fileName) throws Exception {
        InputStream inputStream = TerminalFixFxOneTest.class.getResourceAsStream(fileName);
        
        return new JSONObject(IOUtils.toString(inputStream));
    }
}